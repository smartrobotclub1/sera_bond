const byte r_dir = 2;
const byte r_spd = 3;
const byte l_dir = 5;
const byte l_spd = 6;

const byte RRS=9,RS=10;

void setup() {
  Serial.begin(9600);
pinMode(RRS,INPUT);
pinMode(RS,INPUT);


pinMode(r_dir,OUTPUT);
pinMode(r_spd,OUTPUT);
pinMode(l_dir,OUTPUT);
pinMode(l_spd,OUTPUT);

while(digitalRead(RRS) == 1){
  digitalWrite (r_dir,LOW);  
  analogWrite (r_spd,127);
  digitalWrite (l_dir,LOW);
  analogWrite (l_spd,127);
}
digitalWrite (r_dir,LOW);  
analogWrite (r_spd,255);
digitalWrite (l_dir,HIGH);
analogWrite (l_spd,0);
delay(800);
}
void loop() {
  

if (digitalRead(RRS) == 0){
 left();
}
else{  
  go();
  }
}

void go(){
digitalWrite (r_dir,LOW);  
analogWrite (r_spd,200);
digitalWrite (l_dir,LOW);
analogWrite (l_spd,200);
}
void left(){
digitalWrite (r_dir,LOW);  
analogWrite (r_spd,200);
digitalWrite (l_dir,LOW);
analogWrite (l_spd,0);
}
