const byte r_dir = 2;
const byte r_spd = 3;
const byte l_dir = 5;
const byte l_spd = 6;

const byte RRS=9, RS=10, MS=11, LS=12, LLS=13;

void setup() {
  Serial.begin(9600);
pinMode(LLS,INPUT);
pinMode(LS,INPUT);
pinMode(MS,INPUT);
pinMode(RS,INPUT);
pinMode(RRS,INPUT);

pinMode(r_dir,OUTPUT);
pinMode(r_spd,OUTPUT);
pinMode(l_dir,OUTPUT);
pinMode(l_spd,OUTPUT);


}

void loop() {
  Serial.print(digitalRead(LLS));
  Serial.print(digitalRead(LS));
  Serial.print(digitalRead(MS));
  Serial.print(digitalRead(RS));
  Serial.println(digitalRead(RRS));

  if (digitalRead(LLS)==1 && digitalRead(LS)==1 && digitalRead(MS) == 0 && digitalRead(RS)==1 && digitalRead(RRS)==1 ){//0=line or black
    go();
  }
  if (digitalRead(LS)== 0 && digitalRead(MS) == 1 && digitalRead(RS)==1 ){
    left();
  }
  if (digitalRead(LS)== 1 && (digitalRead(MS)== 1 && digitalRead(RS)==0)){
    right();
  } 
//  -QUICK-BLOCK-

  
  if (digitalRead(LLS)==0 && digitalRead(RRS)==1 ){
  left_q();
  }
  if (digitalRead(LLS)==1 && digitalRead(RRS)==0){
  right_q();
  }
  //-QUICK-BLOCK-
/*
go();
delay(5000);
left();
delay(2000);
left_q();
delay(2000);
right();
delay(2000);
right_q();
delay(2000);
ir();
*/
}

void right(){
digitalWrite (r_dir,LOW);  
analogWrite (r_spd,0);
digitalWrite (l_dir,LOW);
analogWrite (l_spd,255);
}
void left(){
digitalWrite (r_dir,LOW);  
analogWrite (r_spd,255);
digitalWrite (l_dir,LOW);
analogWrite (l_spd,0);
}
void back(){
analogWrite (r_dir,255);  
digitalWrite (r_spd,LOW);
digitalWrite (l_dir,LOW);
analogWrite (l_spd,255);
}
void go(){
digitalWrite (r_dir,LOW);  
analogWrite (r_spd,270);
digitalWrite (l_dir,LOW);
analogWrite (l_spd,270);
}
void left_q(){
digitalWrite (r_dir,LOW);  
analogWrite (r_spd,255);
digitalWrite (l_dir,HIGH);//ЕСЛИ HIGH ТО
analogWrite (l_spd,135);//255-НЕ ЕДЕЬ 0-МАКС СОРОСТЬ
}
void right_q(){
digitalWrite (r_dir,HIGH);  
analogWrite (r_spd,135);
digitalWrite (l_dir,LOW);
analogWrite (l_spd,255);
}
void ir() {
  Serial.print(digitalRead(RS));
  Serial.print(digitalRead(MS));
  Serial.print(digitalRead(LS));
  Serial.print(digitalRead(LLS));
  Serial.println(digitalRead(RRS));
}
