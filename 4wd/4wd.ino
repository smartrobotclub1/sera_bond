const byte r_dir = 2;
const byte r_spd = 3;
const byte l_dir = 4;
const byte l_spd = 5;

const byte RRS=12, RS=11, MS=10, LS=9, LLS=8;

void setup() {
  Serial.begin(9600);
pinMode(RRS,INPUT);
pinMode(RS,INPUT);
pinMode(LLS,INPUT);
pinMode(LS,INPUT);
pinMode(MS,INPUT);

}

void loop() {
  Serial.print(digitalRead(RRS));
  Serial.print(digitalRead(RS));
  Serial.print(digitalRead(MS));
  Serial.print(digitalRead(LS));
  Serial.println(digitalRead(LLS));

  if ( digitalRead(LLS)==1 && digitalRead(LS)==1 && digitalRead(MS) == 0 && digitalRead(RS)==1 && digitalRead(RRS)==1 ){
    go();
  }
  if (digitalRead(LLS)==1 && digitalRead(LS)== 0 && digitalRead(MS)==0 && digitalRead(RS)==1 && digitalRead(RRS)==1 ){
 right();
  }
  if (digitalRead(LLS)==1 && digitalRead(LS)== 1 && digitalRead(MS)== 0 && digitalRead(RS)== 0 && digitalRead(RRS)==1 ){
  left();
  } 
  // delay(1000);
/*
 * go();
delay(1000);
back();
delay(1000);
left();
delay(1000);
right();
delay(1000);
ir();
*/
}


void right(){
  digitalWrite (r_dir,HIGH);  
  digitalWrite (r_spd,LOW);
  digitalWrite (l_dir,HIGH);
  digitalWrite (l_spd,LOW);
}
void left(){
digitalWrite (r_dir,LOW);  
digitalWrite (r_spd,HIGH);
digitalWrite (l_dir,LOW);
digitalWrite (l_spd,HIGH);
}
void back(){
digitalWrite (r_dir,HIGH);  
digitalWrite (r_spd,LOW);
digitalWrite (l_dir,LOW);
digitalWrite (l_spd,HIGH);
}
void go(){
digitalWrite (r_dir,LOW);  
digitalWrite (r_spd,HIGH);
digitalWrite (l_dir,HIGH);
digitalWrite (l_spd,LOW);
}
void ir() {
  Serial.print(digitalRead(RS));
  Serial.print(digitalRead(MS));
  Serial.print(digitalRead(LS));
  Serial.print(digitalRead(LLS));
  Serial.println(digitalRead(RRS));
}
